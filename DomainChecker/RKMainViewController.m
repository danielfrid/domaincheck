//
//  RKMainViewController.m
//  DomainChecker
//
//  Created by IT-Högskolan on 12/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKMainViewController.h"
#import "ViewController.h"
#import "RKSettings.h"
#import <Parse/Parse.h>

@interface RKMainViewController ()
@property (nonatomic) NSMutableArray *tempLoadArray;
@property (weak, nonatomic) IBOutlet UIButton *watchList;
@property (weak, nonatomic) IBOutlet UIButton *updateList;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;

// @property (nonatomic) time
@end

@implementation RKMainViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.tempLoadArray = [[NSMutableArray alloc] init];
    [self setButtonGraphics:self.watchList];
    [self setButtonGraphics:self.updateList];
    [self wakeRKSettings];
    
    //Check if there's been a DL of todays url-file
    if([RKSettings getDate]){
        NSLog(@"viewDidload checks if there's a saved timestamp. Succes!");

        NSUInteger day1 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitEra forDate:[NSDate date]];
        NSLog(@"Day 1: %lu", (unsigned long)day1);
        NSLog(@"Day on time: %@", [NSDate date]);
        NSUInteger day2 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitEra forDate:[RKSettings getDate]];
        NSLog(@"Day 2: %lu", (unsigned long)day2);
        NSLog(@"Day 2 time: %@", [RKSettings getDate]);
        if(day1 > day2){
            NSLog(@"Saved timestamp is older then todays. Start loading parsefile");
            [self loadParseFile];
        }
        else{self.tempLoadArray = [RKSettings getUrlList];}
    }
    else{
        NSLog(@"else in viewDidload. Will load parsefile.");
        [self loadParseFile];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 //Encoda-Decoda NSData och Base64:
 //http://stackoverflow.com/questions/19088231/base64-decoding-in-ios-7
 //
- (void)loadParseFile{
    [self.tempLoadArray removeAllObjects];
    PFQuery *query = [PFQuery queryWithClassName:@"DomainList"];
//  [query getObjectInBackgroundWithId:@"GJTq0SWTUV" block:^(PFObject *domainList, NSError *error){
    [query orderByDescending:@"createdAt"];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *domainList, NSError *error){
        
        NSDate *timestamp = domainList.createdAt; //Spara datum när filen senast uppdaterades så vi inte laddar ner den i onödan från Parse.
        [RKSettings setDate:timestamp]; //sparar nsdate i rksettings
        [RKSettings saveChanges];
        NSLog(@"newNSDate: %@", timestamp);

        NSLog(@"new saved timestamp: %@", [RKSettings getDate]);
        PFFile *theText = domainList [@"urlList"]; //Sparar parse File som PFFile.
        [theText getDataInBackgroundWithBlock:^(NSData *data , NSError *error){
            NSString *decodedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            // Bryter upp string till array med två strängar per plats
            NSArray *tempArray = [decodedString componentsSeparatedByString:@"\n"];
            for(NSString *line in tempArray){
                RKUrl *url = [[RKUrl alloc] init];
                if(line.length != 0){
                    NSRange range = [line rangeOfString:@"\t" options:NSBackwardsSearch];
                    url.urlName = [line substringToIndex:range.location];
                    url.date = [line substringFromIndex:range.location+1];
                    [self.tempLoadArray addObject:url];
                }
            }
            [RKSettings setUrlList:self.tempLoadArray];
            [RKSettings saveChanges];
        }];
    }];
}

- (IBAction)updateFile:(id)sender {
    
    [self loadParseFile];
}
- (IBAction)viewNew:(id)sender {
    
}

/*
 * Set image on UIBarButton.
 */
/*
-(void)setMenuButton:(UIBarButtonItem *)menuButton{
    [menuButton setBackgroundImage:[UIImage imageNamed: @"Bitmap"] forState:UIControlStateNormal barMetrics: UIBarMetricsDefault];
}
*/
-(void)setButtonGraphics:(UIButton *)btn{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:NO];
    [btnLayer setCornerRadius:1.0f];
    [btnLayer setBorderWidth	:1.0f];
    [btnLayer setBackgroundColor:[[UIColor colorWithRed:184.0f / 255.0f green:233.0f / 255.0f blue:134.0f / 255.0f alpha:1.0f] CGColor]];
    
    
    [btnLayer setShadowOpacity:0.6];
    [btnLayer setShadowOffset:CGSizeMake(2.0f, 2.0f)];
    [btnLayer setShadowColor:[[UIColor grayColor] CGColor]];
}

/*
 *  Sätter alla knappar och insällningar
 *  efter användarens sparade referenser.
 */
-(void) wakeRKSettings{
    [RKSettings loadData];
    if(![RKSettings getExist]){
        NSLog(@"No stored data exists. Creating default values.");
        [RKSettings setExist:YES];
        [RKSettings setAmountWord:64];
        [RKSettings setMatchAll:YES];
        [RKSettings setAllowNumbers:NO];
        [RKSettings setSoloKeyWord:NO];
        [RKSettings setStartKeyWord:NO];
    }
    NSLog(@"From Main WakeRK");
    NSLog(@"Exist: %d", [RKSettings getExist]);
    NSLog(@"Allow Nr: %d", [RKSettings getAllowNumbers]);
    NSLog(@"MattchAll: %d", [RKSettings getMatchAll]);
    NSLog(@"SoloKeyW: %d", [RKSettings getSoloKeyWord]);
    NSLog(@"startKeyW: %d", [RKSettings getStartKeyWord]);
    NSLog(@"ANtalBokgs: %ld", (long)[RKSettings getAmountWord]);
    NSLog(@"timestamp: %@", [RKSettings getDate]);
    NSLog(@"urlList length: %lu", (unsigned long)[RKSettings getUrlList].count);
}

/*
 *  Ladda hem domäntexten. Bryt upp filen per radbrytning och lägg det i en tempArray
 *  sedan bryts varje rad upp till en RKUrl ( två stringar. En url och en datum)
 *
 *
 * Används ej mer då vi laddar hem filen från Parse.com 
 
- (void) loadUrls {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^ {
        // Background work here
        NSError *err = nil;
        NSString *url = [[NSString stringWithFormat:@"https://www.iis.se/data/bardate_domains.txt"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *myTxtFile = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:&err];
        if(err != nil) {
            NSLog(@"Fel vid hämtning av data");
        }
        NSArray *tempArray= [myTxtFile componentsSeparatedByString:@"\n"];
        for(NSString *line in tempArray){
           // NSLog(@"Line is : %@", line);
            RKUrl *url = [[RKUrl alloc] init];
            url.urlName = [[line componentsSeparatedByString:@"\t"] objectAtIndex:0];
            if(line.length != 0){
                NSRange range = [line rangeOfString:@"\t" options:NSBackwardsSearch];
                url.date = [line substringFromIndex:range.location+1];
            }
            [self.tempLoadArray addObject:url];
        
        }
        
        NSLog(@"Finished work in background");
        dispatch_async(dispatch_get_main_queue(), ^ {
            NSLog(@"Back on main thread");
           // [self saveUrlList: self.tempLoadArray];
        });
    });
}
*/


/*
 *  Sparar url arrayen lokalt
 */
-(void)saveUrlList:(NSMutableArray *)list {
    NSData *encodedUrls = [NSKeyedArchiver archivedDataWithRootObject:list];
    [[NSUserDefaults standardUserDefaults] setObject:encodedUrls forKey:[NSString stringWithFormat:@"url"]];
    NSLog(@"saved !");
}

/*
 *  Laddar den sparade url arrayen
 */
-(void)loadUrlList{
    NSData *decodedUrls = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"url"]];
    NSArray *decodedArray = [NSKeyedUnarchiver unarchiveObjectWithData:decodedUrls];
    self.urls =[NSMutableArray arrayWithArray:decodedArray];
    NSLog(@"Loaded !");
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"viewNew"]){
        ViewController *view = [ segue destinationViewController];
        view.urls = self.tempLoadArray;
    
    } else if([segue.identifier isEqualToString:@"viewLocal"]){
        ViewController *view = [segue destinationViewController];
        view.urls = self.urls;
        
    }
    else if([segue.identifier isEqualToString:@"tempTable"]){
        ViewController *view = [segue destinationViewController];
        view.urls = self.tempLoadArray;
        
    }
    
}


@end
