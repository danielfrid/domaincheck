//
//  RKTableViewCell.m
//  DomainChecker
//
//  Created by IT-Högskolan on 08/08/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKTableViewCell.h"

@implementation RKTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
