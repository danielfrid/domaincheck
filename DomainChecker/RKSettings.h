//
//  RKSettings.h
//  DomainChecker
//
//  Created by IT-Högskolan on 18/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RKSettings : NSObject
+(void)setExist:(BOOL)xist;
+(BOOL)getExist;
+(void)setAmountWord:(int)words;
+(NSInteger)getAmountWord;
+(void)setMatchAll:(BOOL)match;
+(BOOL)getMatchAll;
+(void)setStartKeyWord:(BOOL)start;
+(BOOL)getStartKeyWord;
+(void)setSoloKeyWord:(BOOL)solo;
+(BOOL)getSoloKeyWord;
+(void)setAllowNumbers:(BOOL)allow;
+(BOOL)getAllowNumbers;
+(void)saveChanges;
+(void)loadData;
+(void)setDate:(NSDate*)date;
+(NSDate*)getDate;
+(void)setUrlList:(NSMutableArray*)theList;
+(NSMutableArray*)getUrlList;
@end