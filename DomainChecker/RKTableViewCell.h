//
//  RKTableViewCell.h
//  DomainChecker
//
//  Created by IT-Högskolan on 08/08/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RKTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *urlName;
@property (weak, nonatomic) IBOutlet UILabel *expDate;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;

@end
