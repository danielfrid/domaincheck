//
//  ViewController.m
//  DomainChecker
//
//  Created by IT-Högskolan on 11/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "ViewController.h"
#import "RKUrl.h"
#import "RKSettings.h"
#import "RKTableViewCell.h"
#import <Parse/Parse.h>

@interface ViewController ()
@property (nonatomic) NSMutableArray *searchedUrls;
@property (nonatomic) BOOL isFiltered;
@property (nonatomic) NSMutableArray *filteredUrls;
@property (nonatomic) BOOL containsNr;
@property (nonatomic) RKTableViewCell *selectedCell;
@property (nonatomic) RKUrl *selectedUrl;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.filteredUrls = self.urls;
    [self urlLength];
    /* Some test from parse.com
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];
     */
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self urlLength];
    [self.tableView reloadData]; // to reload selected cell
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setPushNot:(id)sender {
    
    //Get installationID
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSString *id = currentInstallation.installationId;
    [PFCloud callFunctionInBackground:@"setPush" withParameters:@{@"installationId":id,
                                                                  @"urlDate": self.selectedUrl.date,
                                                                  @"urlName": self.selectedUrl.urlName}];
    
}
#pragma TableView:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isFiltered){
        return [self.searchedUrls count];
    }
    return [self.filteredUrls count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RKCell" forIndexPath:indexPath];

    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor colorWithWhite:33 alpha:0.5]];

    [cell setSelectedBackgroundView:bgColorView];

    RKUrl *url;
    if (cell == nil){
        cell = [[RKTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"RKCell"];
    }
    if(self.isFiltered){
        url = self.searchedUrls[indexPath.row];
    } else {
        url = self.filteredUrls[indexPath.row];
    }
    cell.urlName.text = [NSString stringWithFormat:@"%@", url.urlName];
    cell.expDate.text = [NSString stringWithFormat:@"%@", url.date];
    cell.notificationButton.hidden = YES;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RKTableViewCell *cell = (RKTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.notificationButton.hidden = NO;
    self.selectedCell.notificationButton.hidden = YES;
    self.selectedCell = cell;
    if(!self.isFiltered){
        self.selectedUrl = [self.filteredUrls objectAtIndex:indexPath.row];
    } else {
        self.selectedUrl = [self.searchedUrls objectAtIndex:indexPath.row];
    }
}

#pragma SeachBar:

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.tableView resignFirstResponder];
}
/*
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0){
        self.isFiltered = NO;
    } else {
        self.isFiltered = YES;
        self.searchedUrls = [[NSMutableArray alloc] init];
        for(RKUrl *url in self.filteredUrls){
            NSString *str = url.urlName;
            NSRange stringRange = [str rangeOfString: searchText options:NSCaseInsensitiveSearch];
            if([RKSettings getSoloKeyWord]){
                if([[NSString stringWithFormat:@"%@.se", searchText] isEqualToString:url.urlName]){
                    [self.searchedUrls addObject:url];
                }
            } else if(![RKSettings getSoloKeyWord]){
                        if(stringRange.location != NSNotFound){
                            [self.searchedUrls addObject:url];
                        }
            }
        }
    }
    [self.tableView reloadData];
}
*/
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0){
        self.isFiltered = NO;
    } else {
        self.isFiltered = YES;
        searchText = [searchText lowercaseString];
        self.searchedUrls = [[NSMutableArray alloc] init];
        for(RKUrl *url in self.filteredUrls){
            NSString *str = url.urlName;
            NSRange stringRange = [str rangeOfString: searchText options:NSCaseInsensitiveSearch];
            BOOL matchSearch = (stringRange.location != NSNotFound);
            if([RKSettings getSoloKeyWord]){
                if([[NSString stringWithFormat:@"%@.se", searchText] isEqualToString:url.urlName]){
                    [self.searchedUrls addObject:url];
                }
            } else if(matchSearch){
             //   NSLog(@"matchSearc IF-Loop");
                if(![RKSettings getSoloKeyWord] && [RKSettings getStartKeyWord]){
                    BOOL beginsWith = [str hasPrefix: searchText];  //Koll om den startar med ordet
                    self.containsNr = !([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound); //check if the urls.url contains numbers.
                    
                    if([RKSettings getAllowNumbers] && beginsWith){
                        [self.searchedUrls addObject:url];
                    } else if(![RKSettings getAllowNumbers] && !self.containsNr && beginsWith){
                        [self.searchedUrls addObject:url];
                    }
                }
                else {
                    if([RKSettings getAllowNumbers]){
                        [self.searchedUrls addObject:url];
                    } else if (!(self.containsNr)){
                        [self.searchedUrls addObject:url];
                    }
                }
            }
        }
    }
    [self.tableView reloadData];
}
#pragma searchFilters
/*
 //Filtrera listan till önskad längd på urls och sedan ta bort de med siffror om den variablen är satt.
- (void) urlLength {
    NSLog(@"urlLength method");
    if([RKSettings getAmountWord] > 0){
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for(RKUrl *url in self.urls){
            if (url.urlName.length <= [RKSettings getAmountWord]) {
                if([RKSettings getAllowNumbers]){
                [tempArray addObject:url];
                } else {
                    if ([url.urlName rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound) {
                        [tempArray addObject:url];
                    }
                }
            }
        }
        self.filteredUrls = tempArray;
    }
}
 */

//Filtrera listan till önskad längd på urls.
- (void) urlLength {
    NSLog(@"urlLength method");
    if([RKSettings getAmountWord] > 0){
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for(RKUrl *url in self.urls){
            self.containsNr = !([url.urlName rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound);
            if (url.urlName.length <= [RKSettings getAmountWord]) {
                if(![RKSettings getAllowNumbers] && self.containsNr){}
                else{[tempArray addObject:url];}
                }
        }
        self.filteredUrls = tempArray;
    }
}


- (void) soloKeyWord {
    
    
}
- (void) startKeyWord {}
- (void) endKeyWord {}


@end
