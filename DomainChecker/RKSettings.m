//
//  RKSettings.m
//  DomainChecker
//
//  Created by IT-Högskolan on 18/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKSettings.h"

@interface RKSettings()
@end

@implementation RKSettings
static BOOL exist;
static NSInteger amountOfWord;
static bool allowNumbers;
static bool matchAll;
static bool startKeyWord;
static bool soloKeyWord;
static NSDate* fileDate;
static NSMutableArray* urlList;

+(void)setUrlList:(NSMutableArray*)theList{
    urlList = theList;
}

+(NSMutableArray*)getUrlList{
    return urlList;
}

+(void)setDate:(NSDate *)date{
    fileDate = date;
}

+(NSDate*)getDate{
    return fileDate;
}

+(void)setExist:(BOOL)xist{
    exist = xist;
}

+(BOOL)getExist {
    return exist;
}

+(void)setSoloKeyWord:(BOOL)solo {
    soloKeyWord = solo;
}

+(BOOL)getSoloKeyWord{
    return soloKeyWord;
}

+(void)setStartKeyWord:(BOOL)start{
    startKeyWord = start;
}

+(BOOL)getStartKeyWord {
    return startKeyWord;
}

+(void)setMatchAll:(BOOL)match{
    matchAll = match;
}

+(BOOL)getMatchAll {
    return matchAll;
}

+(void)setAllowNumbers:(BOOL)allow {
    allowNumbers = allow;
}

+(BOOL)getAllowNumbers {
    return allowNumbers;
}

+(void)setAmountWord:(int)words{
    amountOfWord = words;
}

+(NSInteger)getAmountWord {
    return amountOfWord;
}

+(void)saveChanges {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:exist forKey:@"exist"];
    [prefs setInteger: amountOfWord forKey:@"amountOfWord"];
    [prefs setBool:matchAll forKey:@"matchAll"];
    [prefs setBool:startKeyWord forKey:@"startKeyWord"];
    [prefs setBool:allowNumbers forKey:@"allowNumbers"];
    [prefs setBool:soloKeyWord forKey:@"soloKeyWord"];
    [prefs setObject:fileDate forKey:@"fileDate"];
    NSData *encodedUrls = [NSKeyedArchiver archivedDataWithRootObject:urlList];
    [[NSUserDefaults standardUserDefaults] setObject:encodedUrls forKey:[NSString stringWithFormat:@"urlList"]];
    NSLog(@"RKSetting is saved!");
}

+(void)loadData{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    exist = [prefs boolForKey:@"exist"];
    amountOfWord = [prefs integerForKey:@"amountOfWord"];
    matchAll = [prefs boolForKey:@"matchAll"];
    startKeyWord = [prefs boolForKey:@"startKeyWord"];
    allowNumbers = [prefs boolForKey:@"allowNumbers"];
    soloKeyWord = [prefs boolForKey:@"soloKeyWord"];
    fileDate = [prefs objectForKey:@"fileDate"];
    if(exist){
        NSData *decodedUrls = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"urlList"]];
        NSArray *decodedArray = [NSKeyedUnarchiver unarchiveObjectWithData:decodedUrls];
        urlList =[NSMutableArray arrayWithArray:decodedArray];
    }

    NSLog(@"Stored data is loaded with following data:");
    NSLog(@"Exist: %d", [RKSettings getExist]);
    NSLog(@"Allow Nr: %d", [RKSettings getAllowNumbers]);
    NSLog(@"MattchAll: %d", [RKSettings getMatchAll]);
    NSLog(@"SoloKeyW: %d", [RKSettings getSoloKeyWord]);
    NSLog(@"startKeyW: %d", [RKSettings getStartKeyWord]);
    NSLog(@"ANtalBokgs: %ld", (long)[RKSettings getAmountWord]);
    NSLog(@"Timestamp is: %@", [RKSettings getDate]);
    NSLog(@"urlList lenght: %lu", (unsigned long)urlList.count);
    
}

+ (id)alloc {
    [NSException raise:@"Cannot be instantiated!" format:@"Static class 'ClassName' cannot be instantiated!"];
    return nil;
}


@end

