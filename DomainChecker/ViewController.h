//
//  ViewController.h
//  DomainChecker
//
//  Created by IT-Högskolan on 11/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property  (nonatomic) NSMutableArray *urls;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

