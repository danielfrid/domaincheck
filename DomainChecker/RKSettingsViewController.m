//
//  RKSettingsViewController.m
//  DomainChecker
//
//  Created by IT-Högskolan on 17/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKSettingsViewController.h"
#import "RKSettings.h"

@interface RKSettingsViewController ()
@property (weak, nonatomic) IBOutlet UISlider *wordSlider;
@property (weak, nonatomic) IBOutlet UISwitch *soloKeyWordSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *startKeyWordSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *noNumberWordSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *matchAllKeyWordSwitch;
@property (weak, nonatomic) IBOutlet UILabel *amountOfCharLabel;
@property (weak, nonatomic) IBOutlet UILabel *veryLongCharSerie;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *contactView;
@property (weak, nonatomic) IBOutlet UIButton *buttonC;
@property (weak, nonatomic) IBOutlet UIButton *buttonB;
@property (weak, nonatomic) IBOutlet UIButton *buttonQ;
@property (nonatomic) NSMutableArray *veryLongCharArray;


@end

@implementation RKSettingsViewController
- (IBAction)contactView:(id)sender {
    [self.contactView setHidden:false];
    [self.infoView setHidden:YES];
    [self.filterView setHidden:YES];

}
- (IBAction)infoView:(id)sender {
    [self.infoView setHidden:false];
    [self.filterView setHidden:YES];
    [self.contactView setHidden:YES];
}
- (IBAction)filterView:(id)sender {
    [self.filterView setHidden:false];
    [self.contactView setHidden:YES];
    [self.infoView setHidden:YES];
    [self.view reloadInputViews];
    NSLog(@"Clocked");
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)matchAllKeyWord:(id)sender {
    if([sender isOn]){
        [RKSettings setMatchAll:YES];
        [RKSettings setSoloKeyWord:NO];
        [RKSettings setStartKeyWord:NO];
        [self.soloKeyWordSwitch setOn:NO animated:YES];
        [self.startKeyWordSwitch setOn:NO animated:YES];
    } else {
        [RKSettings setMatchAll:NO];
    }
}

- (IBAction)soloKeyWord:(id)sender {
    if([sender isOn]){
        [RKSettings setSoloKeyWord:YES];
        [RKSettings setMatchAll:NO];
        [RKSettings setStartKeyWord:NO];
        [RKSettings setAllowNumbers:YES];
        [self.noNumberWordSwitch setOn:YES];
        [self.startKeyWordSwitch setOn:NO animated:YES];
        [self.matchAllKeyWordSwitch setOn:NO animated:YES];
    } else {
        [RKSettings setSoloKeyWord:NO];
    }
}
- (IBAction)startKeyWord:(id)sender {
    if([sender isOn]){
        [RKSettings setStartKeyWord:YES];
        [RKSettings setSoloKeyWord:NO];
        [RKSettings setMatchAll:NO];
        [self.soloKeyWordSwitch setOn:NO animated:YES];
        [self.matchAllKeyWordSwitch setOn:NO animated:YES];
    } else {
        [RKSettings setStartKeyWord:NO];
    }

}
- (IBAction)noNumberWord:(id)sender {
    if([sender isOn]){
        [RKSettings setAllowNumbers:YES];
        [RKSettings setSoloKeyWord:NO];
        [self.soloKeyWordSwitch setOn:NO animated:YES];
    } else {
        [RKSettings setAllowNumbers:NO];
    }

}


- (IBAction)saveSettings:(id)sender {
    [RKSettings saveChanges];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Saving settings");
}

- (IBAction)cancel:(id)sender {
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"cancel");
}

- (IBAction)numOfChars:(id)sender {
    NSUInteger index = (NSUInteger)(self.wordSlider.value + 0.5);
    [self.wordSlider setValue:index animated:NO];
    self.amountOfCharLabel.text = [NSString stringWithFormat:@"Antal tecken: %d", (int)roundf(self.wordSlider.value)-3];
    [RKSettings setAmountWord:((int)roundf(self.wordSlider.value))];
    [self setLongCharLabel];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setControlButtons:self.buttonB];
    [self setControlButtons:self.buttonQ];
    [self setControlButtons:self.buttonC];
    [self setControllerValues];
    [self setUserPref];
    [self setLongArray];
    [self setLongCharLabel];
    self.veryLongCharSerie.textColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:1];
}

- (void) setControlButtons: (UIButton*) btn{
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:NO];
    [btnLayer setShadowOpacity:0.6];
    [btnLayer setShadowOffset:CGSizeMake(2.0f, 2.0f)];
    [btnLayer setShadowColor:[[UIColor grayColor] CGColor]];
    [btnLayer setCornerRadius:15.0f];
    [btnLayer setBorderWidth:0.0f];
    [btnLayer setBackgroundColor:[[UIColor colorWithRed:184.0f / 255.0f green:233.0f / 255.0f blue:134.0f / 255.0f alpha:1.0f] CGColor]];
}

-(void) setControllerValues {
    self.wordSlider.minimumValue = 5;
    self.wordSlider.maximumValue = 67;
}

-(void)setUserPref {
    if(!(self.wordSlider.value = (float)[RKSettings getAmountWord])>0){
        self.wordSlider.value = self.wordSlider.maximumValue;
    }
    self.amountOfCharLabel.text = [NSString stringWithFormat:@"Antal tecken: %d", (int)roundf(self.wordSlider.value)-3];
    
    [self.soloKeyWordSwitch setOn:[RKSettings getSoloKeyWord]];
    [self.matchAllKeyWordSwitch setOn:[RKSettings getMatchAll]];
    [self.startKeyWordSwitch setOn:[RKSettings getStartKeyWord]];
    [self.noNumberWordSwitch setOn:[RKSettings getAllowNumbers]];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLongArray {
    NSString *word = @"enbrasajtskallhaenvettigurlsomintharmycketteckendetvetduva-12345---";
    self.veryLongCharArray = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < word.length; i++) {
        NSRange range = NSMakeRange(0, i);
        NSString *temp = [word substringWithRange:range];
        [self.veryLongCharArray addObject:temp];
    }
}

- (void) setLongCharLabel{
    self.veryLongCharSerie.text = [NSString stringWithFormat:@"%@.se", self.veryLongCharArray[(int)self.wordSlider.value-3]];

}

@end
