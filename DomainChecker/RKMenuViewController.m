//
//  RKMenuViewController.m
//  DomainChecker
//
//  Created by IT-Högskolan on 24/07/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKMenuViewController.h"

@interface RKMenuViewController ()
@property (weak, nonatomic) IBOutlet UIView *menuOne;
@property (weak, nonatomic) IBOutlet UIView *menu2;

@end

@implementation RKMenuViewController

- (void)viewWillLayoutSubviews{
    [self setStartViewSize];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setStartViewSize{
        NSLog(@"makesum");
    CGRect menuOneSmall = self.menuOne.frame;
    NSLog(@"height:%f", menuOneSmall.size.height);
    menuOneSmall.size.height = 10;

    [self.menuOne setFrame:menuOneSmall];
    NSLog(@"height:%f", menuOneSmall.size.height);
    
    CGRect menuTwoSmall = self.menu2.frame;
    menuTwoSmall.size.height = 2;
    [self.menuOne setFrame: menuTwoSmall];
    [self.menuOne setBackgroundColor:[UIColor redColor]];
    NSLog(@"height:%f", self.menu2.frame.size.height);
//    self.menu2.frame = menuTwoSmall;
}
                          
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
