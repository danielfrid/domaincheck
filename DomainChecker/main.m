//
//  main.m
//  DomainChecker
//
//  Created by IT-Högskolan on 11/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
