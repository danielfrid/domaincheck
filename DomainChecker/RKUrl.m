//
//  RKUrl.m
//  DomainChecker
//
//  Created by IT-Högskolan on 11/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import "RKUrl.h"
@interface RKUrl()
@end

@implementation RKUrl
-(id) initWithCoder:(NSCoder *) decoder{
    self = [super init];
    if(!self){
        return nil;
    }
    self.urlName = [decoder decodeObjectForKey:@"urlName"];
    self.date = [decoder decodeObjectForKey:@"date"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.urlName forKey:@"urlName"];
    [encoder encodeObject:self.date forKey:@"date"];
}

@end
