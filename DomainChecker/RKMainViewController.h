//
//  RKMainViewController.h
//  DomainChecker
//
//  Created by IT-Högskolan on 12/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RKUrl.h"

@interface RKMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *loadLocalFile;
@property (weak, nonatomic) IBOutlet UIButton *updateFile;
@property (nonatomic) NSMutableArray *urls;

@end
