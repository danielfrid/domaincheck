//
//  RKUrl.h
//  DomainChecker
//
//  Created by IT-Högskolan on 11/05/15.
//  Copyright (c) 2015 Roadkill. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RKUrl : NSObject
@property (nonatomic) NSString *urlName;
@property (nonatomic) NSString *date;

@end
