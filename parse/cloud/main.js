Parse.Cloud.job("fetchUrlList", function(request, response) {
    Parse.Cloud.httpRequest({ url: 'https://www.iis.se/data/bardate_domains.txt'}).then( function(httpResponse){
   	    var Buffer = require('buffer').Buffer; //import Node.js Buffer

        list = httpResponse.text;
        var buf = new Buffer(list).toString('base64');
        var file = new Parse.File("urlList.txt", { base64: buf });
        var fileArray = list.split('\n');
        var DomainList = Parse.Object.extend("DomainList");
	    var domainList = new DomainList();

	    domainList.set("urlList", file);
	    domainList.save(null, { success: function(domainList){
	        			console.log("File saved.");
			/*
			var query = new Parse.Query("DomainList");
			query.select('urlList', {
			success: function(object){
				console.log("success!");
			},
			error: function(object, error){
				console.log("no success!");
			}
			})*/
	        		
	        	}, error: function(domainList, error){
	        		alert("Fail");
	        		console.log(error.message);
	        	}

	        	});
        });

    //check if any pushnotification should launch
    //make a date set one day ahead
    var today = new Date();
    var tomorrow = new Date(today);
    tomorrow.setDate(today.getDate()+1);
    var d = dateToString(tomorrow);
   	
   	var Query = Parse.Object.extend("scheduledPush");
    var query = new Parse.Query(Query);
    query.equalTo("urlDate", d);
    query.find({
    	success: function(results){
    		//var today = new Date("2015-08-17");
    		console.log("query length= "+results.length)
    		for(var i = 0; i < results.length; i++){
    			//var expDate = new Date(results[i].get("urlDate"));
    			//if(dateDiffInDays(today, expDate) == 1){
    			 console.log("Need a Push"); 
    			 var msg = " blir fri inom 24 timmar!"
    			 var id = results[i].get("installationId");
    			 var url = results[i].get("urlName");
    			 var dat = results[i].get("urlDate");
    			 sendPush(id, msg, url, dat);
    			//}
    		}
    	},
    	error: function(error){
    		console.log("Q-error: "+error.message);
    	}
    });

 	function dateDiffInDays(a, b) {
 		var _MS_PER_DAY = 1000 * 60 * 60 * 24;
  		// Discard the time and time-zone information.
  		var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  		var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  		return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}

	function dateToString(value){
		var y = value.getFullYear();
    	var m = value.getMonth()+1;
        if(m <10){m="0"+m;}
    	var d = value.getDate();
        if(d<10){ d="0"+d;}
   		return y+"-"+m+"-"+d;
	}

	function sendPush(id, msg, url, date){
		var message = url+msg+"("+date+")";
		var pushQuery = new Parse.Query(Parse.Installation);
		pushQuery.equalTo('installationId', id);
		 
		  Parse.Push.send({
		    where: pushQuery,
		    data: {
		      alert: message
		    }
		  }, {
		    success: function() {
		      // Push was successful
		    },
		    error: function(error) {
		      throw "Got an error " + error.code + " : " + error.message;
		    }
		  });

		}
});

Parse.Cloud.define("download", function(request, response){
	var query = new Parse.Query("DomainList");
	query.select('urlList', {
		success: function(object){
			console.log("success!");
		},
		error: function(object, error){
			console.log("no success!");
		}
	})

});

Parse.Cloud.define("setPush", function(request, response) {
	var id = request.params.installationId;
 	var urlDate = request.params.urlDate;
 	var urlName = request.params.urlName;
 
 
  var SchedulePush = Parse.Object.extend("scheduledPush");
  var schedulePush = new SchedulePush;
  schedulePush.set("installationId", id);
  schedulePush.set("urlDate", urlDate);
  schedulePush.set("urlName", urlName);
  schedulePush.save(null, {
  	success: function(schedulePush){
  		alert("saved schedulePush");
  	},
  	error: function(schedulePush, error){
  		alert("error: " + error.message);
  	}
  });

/*
//Some kind of test-search:
var Query = Parse.Object.extend("scheduledPush");
    	var query = new Parse.Query(Query);
    	query.equalTo("urlDate", "2015-08-18");
    	query.find({
    		success: function(results){
    			console.log("Q-results: "+results[0].get("urlDate"));
    		},
    		error: function(error){
    			console.log("Q-error: "+error.message);
    		}
    	});

  //Push:
  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo('installationId', '992aadd5-c9eb-45a9-9530-149edf4852f9');
    
  Parse.Push.send({
    where: pushQuery, // Set our Installation query
    data: {
      alert: "New comment: "
    }
  }, {
    success: function() {
      // Push was successful
    },
    error: function(error) {
      throw "Got an error " + error.code + " : " + error.message;
    }
  });
*/

});



